<?php

namespace Tests\Unit;

use App\Models\Card;
use Tests\TestCase;

class CardTest extends TestCase
{
    /**
     * @var Card
     */
    protected $modelCard;
    private $suits = ['S', 'D', 'H', 'C'];
    private $ranks = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K'];
    private $cards = [
        "SA",
        "S2",
        "S3",
        "S4",
        "S5",
        "S6",
        "S7",
        "S8",
        "S9",
        "S10",
        "SJ",
        "SQ",
        "SK",
        "DA",
        "D2",
        "D3",
        "D4",
        "D5",
        "D6",
        "D7",
        "D8",
        "D9",
        "D10",
        "DJ",
        "DQ",
        "DK",
        "HA",
        "H2",
        "H3",
        "H4",
        "H5",
        "H6",
        "H7",
        "H8",
        "H9",
        "H10",
        "HJ",
        "HQ",
        "HK",
        "CA",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "C9",
        "C10",
        "CJ",
        "CQ",
        "CK",
    ];

    protected function setUp()
    {
        $this->modelCard = new Card();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInstanceClass()
    {
        $this->assertInstanceOf(
            Card::class,
            $this->modelCard
        );
    }

    public function testGetCardsArray()
    {
        $this->assertTrue(is_array($this->modelCard->getCards()));
    }

    public function testGetSuitsArray()
    {
        $this->assertTrue(is_array($this->modelCard->getSuits()));
    }

    public function testGetRanksArray()
    {
        $this->assertTrue(is_array($this->modelCard->getRanks()));
    }

    public function testGetCardsHas()
    {
        $this->assertTrue(json_encode($this->cards) === json_encode($this->modelCard->getCards()));
    }

    public function testGetSuitsHas()
    {
        $this->assertTrue(json_encode($this->suits) === json_encode($this->modelCard->getSuits()));
    }

    public function testGetRanksHas()
    {
        $this->assertTrue(json_encode($this->ranks) === json_encode($this->modelCard->getRanks()));
    }

    public function testMatchCard()
    {
        $i = array_rand($this->modelCard->getSuits());
        $j = array_rand($this->modelCard->getRanks());
        $suit = $this->modelCard->getSuits()[$i];
        $rank = $this->modelCard->getRanks()[$j];
        $this->assertTrue(in_array($suit . $rank, $this->modelCard->getCards()));
    }
}
