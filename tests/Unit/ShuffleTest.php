<?php

namespace Tests\Unit;

use App\Models\Card;
use App\Services\ShuffleCards;
use Tests\TestCase;

class ShuffleTest extends TestCase
{
    private $cards = [
        "SA",
        "S2",
        "S3",
        "S4",
        "S5",
        "S6",
        "S7",
        "S8",
        "S9",
        "S10",
        "SJ",
        "SQ",
        "SK",
        "DA",
        "D2",
        "D3",
        "D4",
        "D5",
        "D6",
        "D7",
        "D8",
        "D9",
        "D10",
        "DJ",
        "DQ",
        "DK",
        "HA",
        "H2",
        "H3",
        "H4",
        "H5",
        "H6",
        "H7",
        "H8",
        "H9",
        "H10",
        "HJ",
        "HQ",
        "HK",
        "CA",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "C9",
        "C10",
        "CJ",
        "CQ",
        "CK",
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetCardsArray()
    {
        $this->assertTrue(is_array(ShuffleCards::getCards()));
    }

    public function testGetCardsInSession()
    {
        $this->withSession(['shuffledPile' => $this->cards])
            ->assertTrue(is_array(ShuffleCards::getCards()));
    }

    public function testGetCardsNotDiff()
    {
        $this->assertCount(0, array_diff(ShuffleCards::getCards(), $this->cards));
    }

    public function testGetCardsDiff()
    {
        $cards = $this->cards;
        array_shift($cards);
        $this->assertCount(1, array_diff(ShuffleCards::getCards(), $cards));
    }

    public function testDropCard()
    {
        $cards = ShuffleCards::getCards();
        $this->assertCount(count($cards) - 1, ShuffleCards::dropCard($cards));
    }

    public function testGetPercentChanceFloat()
    {
        $this->assertTrue(is_float(ShuffleCards::getPercentChance($this->cards)));
    }

    public function testGetPercentChanceValue()
    {
        $cards = ShuffleCards::getCards();
        $cards = ShuffleCards::dropCard($cards);
        $this->withSession(['shuffledPile' => $cards]);
        $cards = ShuffleCards::dropCard($cards);
        $this->withSession(['shuffledPile' => $cards]);
        $cards = ShuffleCards::dropCard($cards);

        $this->withSession(['shuffledPile' => $cards])
            ->assertEquals(2.04, ShuffleCards::getPercentChance($cards));
    }

    public function testGetPercentChanceMatch()
    {
        $modelCard = new Card();
        $selectedCard = 'SK';
        $cards = $modelCard->getCards();
        $position = array_search($selectedCard, $cards);

        $currentCard = null;
        for ($k = 1; $k <= $position; $k++) {
            $this->withSession(['shuffledPile' => $cards]);
            $cards = ShuffleCards::dropCard($cards);
            $currentCard = reset($cards);
        }

        $this->assertEquals(
            $selectedCard,
            $currentCard
        );
    }
}
