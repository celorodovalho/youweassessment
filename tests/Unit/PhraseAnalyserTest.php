<?php

namespace Tests\Unit;

use App\Services\PhraseAnalyser;
use Tests\TestCase;

class PhraseAnalyserTest extends TestCase
{
    private $phrase = 'football vs soccer';
    private $phraseSpliced = [
        "f" => 1,
        "o" => 3,
        "t" => 1,
        "b" => 1,
        "a" => 1,
        "l" => 2,
        " " => 2,
        "v" => 1,
        "s" => 2,
        "c" => 2,
        "e" => 1,
        "r" => 1,
    ];
    private $phraseBfAf = [
        "f" => [
            "after" => [
                0 => "none",
            ],
            "before" => [
                "o" => "o",
            ],
        ],
        "o" => [
            "after" => [
                "f" => "f",
                "o" => "o",
                "s" => "s",
            ],
            "before" => [
                "o" => "o",
                "t" => "t",
                "c" => "c",
            ],
        ],
        "t" => [
            "after" => [
                "o" => "o",
            ],
            "before" => [
                "b" => "b",
            ],
        ],
        "b" => [
            "after" => [
                "t" => "t",
            ],
            "before" => [
                "a" => "a",
            ],
        ],
        "a" => [
            "after" => [
                "b" => "b",
            ],
            "before" => [
                "l" => "l",
            ],
        ],
        "l" => [
            "after" => [
                "a" => "a",
                "l" => "l",
            ],
            "before" => [
                "l" => "l",
                " " => " ",
            ],
        ],
        " " => [
            "after" => [
                "l" => "l",
                "s" => "s",
            ],
            "before" => [
                "v" => "v",
                "s" => "s",
            ],
        ],
        "v" => [
            "after" => [
                " " => " ",
            ],
            "before" => [
                "s" => "s",
            ],
        ],
        "s" => [
            "after" => [
                "v" => "v",
                " " => " ",
            ],
            "before" => [
                " " => " ",
                "o" => "o",
            ],
        ],
        "c" => [
            "after" => [
                "o" => "o",
                "c" => "c",
            ],
            "before" => [
                "c" => "c",
                "e" => "e",
            ],
        ],
        "e" => [
            "after" => [
                "c" => "c",
            ],
            "before" => [
                "r" => "r",
            ],
        ],
        "r" => [
            "after" => [
                "e" => "e",
            ],
            "before" => [
                0 => "none",
            ],
        ],
    ];
    private $phraseDistance = [
        "o" => 10,
        "l" => 10,
        " " => 8,
        "s" => 10,
        "c" => 14,
    ];
    private $phraseGraphic = 'f : 1 : before:o after:none
o : 3 : before:o,t,c after:f,o,s max-distance: 10 chars
t : 1 : before:b after:o
b : 1 : before:a after:t
a : 1 : before:l after:b
l : 2 : before:l,  after:a,l max-distance: 10 chars
  : 2 : before:v,s after:l,s max-distance: 8 chars
v : 1 : before:s after: 
s : 2 : before: ,o after:v,  max-distance: 10 chars
c : 2 : before:c,e after:o,c max-distance: 14 chars
e : 1 : before:r after:c
r : 1 : before:none after:e
';

    public function testCountCharOccurrencesArray()
    {
        $split = str_split($this->phrase);
        $this->assertTrue(is_array(PhraseAnalyser::countCharOccurrences($this->phrase, $split)));
    }

    public function testCountCharOccurrences()
    {
        $split = str_split($this->phrase);
        $result = PhraseAnalyser::countCharOccurrences($this->phrase, $split);
        $this->assertEquals(json_encode($this->phraseSpliced), json_encode($result));
    }

    public function testGetBeforeAfterChars()
    {
        $split = str_split($this->phrase);
        $result = PhraseAnalyser::getBeforeAfterChars($this->phrase, $split);
        $this->assertEquals(json_encode($this->phraseBfAf), json_encode($result));
    }

    public function testAnalyser()
    {
        $this->assertEquals($this->phraseGraphic, PhraseAnalyser::analyser($this->phrase));
    }

    public function testGetDistanceBetweenMultiChars()
    {
        $split = str_split($this->phrase);
        $result = PhraseAnalyser::countCharOccurrences($this->phrase, $split);
        $result = PhraseAnalyser::getDistanceBetweenMultiChars($this->phrase, $result);
        $this->assertEquals(json_encode($this->phraseDistance), json_encode($result));
    }
}
