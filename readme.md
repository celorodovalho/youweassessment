Create:
```
<VirtualHost *:80>
    DocumentRoot "/var/www/html/youweassessment/public"
    ServerName youwe.assessment
    ServerAlias youwe.assessment

	<Directory "/var/www/html/youweassessment/public">
        AllowOverride All
	</Directory>
</VirtualHost>
```

Add to /etc/hosts:
```
127.0.0.1 youwe.assessment
```

Run:
```
composer install
cp .env.example .env
php artisan key:generate
chmod -R 777 storage/
chmod -R 777 bootstrap/cache/
./vendor/bin/phpunit 
```

Access:
```
http://youwe.assessment/
http://youwe.assessment/phrase
```

Files:
```
app/Http/Controllers/PhraseAnalyser/PhraseAnalyserController.php
app/Http/Controllers/Poker/PlayController.php
app/Models/Card.php
app/Services/PhraseAnalyser.php
app/Services/ShuffleCards.php
resources/views/
routes/web.php
tests/Unit/CardTest.php
tests/Unit/PhraseAnalyserTest.php
tests/Unit/ShuffleTest.php
```