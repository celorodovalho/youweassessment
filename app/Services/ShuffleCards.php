<?php

namespace App\Services;

use App\Models\Card;
use Illuminate\Support\Facades\Session;

class ShuffleCards
{
    private static function shuffle(&$cards)
    {
        shuffle($cards);
    }

    private static function registerCards($cards)
    {
        Session::put(['shuffledPile' => $cards]);
        Session::save();
    }

    public static function getCards()
    {
        if (Session::has('shuffledPile')) {
            return Session::get('shuffledPile');
        } else {
            $cardModel = new Card();
            $cards = $cardModel->getCards();
            self::shuffle($cards);
            self::registerCards($cards);
            return $cards;
        }
    }

    public static function dropCard($cards)
    {
        array_shift($cards);
        self::registerCards($cards);
        return $cards;
    }

    public static function getPercentChance($cards)
    {
        return floatval(number_format((1 / count($cards)) * 100, 2));
    }
}
