<?php

namespace App\Services;


class PhraseAnalyser
{
    public static function analyser($phrase)
    {
        $chars = str_split($phrase);
        $charsCount = self::countCharOccurrences($phrase, $chars);
        $bfChars = self::getBeforeAfterChars($phrase, $chars);
        $distance = self::getDistanceBetweenMultiChars($phrase, $charsCount);
        return self::formatResult($charsCount, $bfChars, $distance);
    }

    public static function countCharOccurrences($phrase, $chars)
    {
        $counts = [];
        foreach ($chars as $char) {
            if (!empty($char)) {
                $counts[$char] = substr_count($phrase, $char);
            }
        }
        return $counts;
    }

    public static function getBeforeAfterChars($phrase, $chars)
    {
        $positions = [];
        $lastPos = 0;
        foreach ($chars as $char) {
            while (($lastPos = strpos($phrase, $char, $lastPos)) !== false) {
                if ($lastPos > 0) {
                    $after = substr($phrase, $lastPos - 1, 1);
                    if (!empty($after)) {
                        $positions[$char]['after'][$after] = $after;
                    } else {
                        $positions[$char]['after'] = ['none'];
                    }
                } else {
                    $positions[$char]['after'] = ['none'];
                }
                if ($lastPos < strlen($phrase)) {
                    $before = substr($phrase, $lastPos + 1, 1);
                    if (!empty($before)) {
                        $positions[$char]['before'][$before] = $before;
                    } else {
                        $positions[$char]['before'] = ['none'];
                    }
                } else {
                    $positions[$char]['before'] = ['none'];
                }
                $lastPos = $lastPos + strlen($char);
            }
        }
        return $positions;
    }

    public static function getDistanceBetweenMultiChars($phrase, $charsCount)
    {
        $distance = [];
        foreach ($charsCount as $char => $count) {
            if ($count > 1) {
                $divided = explode($char, $phrase);
                $distance[$char] = max(array_map('strlen', $divided));
            }
        }
        return $distance;
    }

    private static function formatResult($charsCount, $bfChars, $distance)
    {
        $analysisStr = '';
        foreach ($charsCount as $char => $count) {
            $analysis[$char] = [];
            $analysisStr .= $char . ' : ';
            if (array_key_exists($char, $charsCount)) {
                $analysisStr .= $charsCount[$char] . ' : ';
            }
            if (array_key_exists($char, $bfChars)) {
                $analysisStr .= 'before:' . implode(',', $bfChars[$char]['before']);
                $analysisStr .= ' after:' . implode(',', $bfChars[$char]['after']);
            }
            if (array_key_exists($char, $distance)) {
                $analysisStr .= ' max-distance: ' . $distance[$char] . ' chars';
            }
            $analysisStr .= PHP_EOL;
        }
        return $analysisStr;
    }
}
