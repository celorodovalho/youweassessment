<?php

namespace App\Http\Controllers\Poker;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Services\ShuffleCards;
use Illuminate\Http\Request;

class PlayController extends Controller
{
    function __construct()
    {
        $this->middleware('web');
    }

    public function index(Request $request)
    {
        $request->session()->flush();
        if ($request->isMethod('post')) {
            return redirect()->route('play', [
                'suit' => $request->input('suit'),
                'rank' => $request->input('rank'),
            ]);
        }
        $cardModel = new Card();
        return view('poker/index', [
            'ranks' => $cardModel->getRanks(),
            'suits' => $cardModel->getSuits(),
        ]);
    }

    public function play(Request $request, $suit, $rank)
    {
        $cards = ShuffleCards::getCards();
        if ($request->isMethod('post')) {
            $cards = ShuffleCards::dropCard($cards);
        }
        return view('poker/play', [
            'selected' => $suit . $rank,
            'cards' => $cards,
            'chance' => ShuffleCards::getPercentChance($cards),
        ]);
    }
}
