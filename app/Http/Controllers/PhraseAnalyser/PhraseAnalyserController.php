<?php

namespace App\Http\Controllers\PhraseAnalyser;

use App\Http\Controllers\Controller;
use App\Services\PhraseAnalyser;
use Illuminate\Http\Request;

class PhraseAnalyserController extends Controller
{
    public function index(Request $request)
    {
        $analysis = null;
        $phrase = null;
        if ($request->isMethod('post')) {
            $phrase = $request->input('phrase');
            $analysis = PhraseAnalyser::analyser($phrase);
        }
        return view('phrase_analyser/index', [
            'analysis' => $analysis,
            'phrase' => $phrase,
        ]);
    }
}
