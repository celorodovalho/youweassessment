<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    private $suits = ['S', 'D', 'H', 'C'];
    private $ranks = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K'];

    public function getCards()
    {
        $cards = [];
        foreach ($this->suits as $suit) {
            foreach ($this->ranks as $rank) {
                $cards[] = $suit . $rank;
            }
        }
        return $cards;
    }

    public function getSuits()
    {
        return $this->suits;
    }

    public function getRanks()
    {
        return $this->ranks;
    }
}
