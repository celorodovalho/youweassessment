@extends('layouts.default')
@section('content')
    <h3>Test 2: Phrase analyser</h3>
    {!! Form::open() !!}
    <div class="form-group">
        {{ Form::label('phrase', 'Insert the word/phrase, please:', ['class' => 'control-label']) }}
        {{ Form::text('phrase', null, ['class' => 'form-control', 'maxlength' => 255]) }}
        @if ($analysis)
            <pre>{{ $analysis }}</pre>
        @endif
    </div>
    {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
    {!! Form::close() !!}
@endsection