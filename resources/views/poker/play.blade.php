@extends('layouts.default')
@section('content')
    <h3>Test 1: Poker chance calculator</h3>
    {!! Form::open(['method' => 'post']) !!}
    <div class="form-group">
        <h4>Selected card: {{ $selected }}</h4>
    </div>
    <div class="form-group">
        @if(!empty($cards))
            <h4>Current card: {{ reset($cards) }}</h4>
            <h4>Chance of getting selected card on the next Draft.: {{ number_format($chance, 2) }}%</h4>
        @else
            <h4>Empty deck</h4>
        @endif
    </div>
    {{ Form::submit('Next', ['class' => 'btn btn-success']) }}
    <a href='{!! url('/') !!}' class="btn btn-warning">Try again</a>
    {!! Form::close() !!}
    <script>
        var currentCard = '{{ reset($cards) }}',
            selectedCard = '{{ $selected }}';
        if (currentCard === selectedCard) {
            var response = confirm("Got it, the chance was {{ number_format($chance, 2) }}%");
            if (response) {
                window.location = "{!! url('/') !!}";
            }
        }
    </script>
@endsection