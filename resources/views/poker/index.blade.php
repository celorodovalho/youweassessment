@extends('layouts.default')
@section('content')
    <h3>Test 1: Poker chance calculator</h3>
    {!! Form::open(['action' => 'Poker\PlayController@index']) !!}
    <div class="form-group">
        {{ Form::label(null, 'Select the Suit, please:', ['class' => 'control-label']) }}
        <br>
        @foreach ($suits as $suit)
            <label class="radio-inline">
                {{ Form::radio('suit', $suit, ['required' => 'required']) }} {{ $suit }}
            </label>
        @endforeach
    </div>
    <div class="form-group">
        {{ Form::label(null, 'Select the Rank, please:', ['class' => 'control-label']) }}
        <br>
        @foreach ($ranks as $rank)
            <label class="radio-inline">
                {{ Form::radio('rank', $rank, ['required' => 'required']) }} {{ $rank }}
            </label>
        @endforeach
    </div>
    {{ Form::submit('Next', ['class' => 'btn btn-success']) }}
    {!! Form::close() !!}
@endsection